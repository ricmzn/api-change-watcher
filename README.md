* Edit .gitlab-ci.yml to change the url in the test job
* Update example.json with formatted output (from `curl $URL | jq . > example.json`)
* Configure scheduled pipeline as per https://docs.gitlab.com/ee/ci/pipelines/schedules.html
* Make sure your GitLab user account has email notifications enabled
* Commit and push
